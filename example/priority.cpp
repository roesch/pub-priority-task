//
// an example usage of tasks with randomly assigned priorities over a
// particular distribution
//

#include <algorithm>
#include <array>
#include <chrono>
#include <cstddef>
#include <functional>
#include <iostream>
#include <mutex>
#include <random>
#include <thread>
#include <utility>
#include "priority_task.hpp"

constexpr std::size_t initial_count = 100;
constexpr std::size_t thread_count = 4;

static dsa::priority_task_system <unsigned> task_queue (thread_count);
static std::geometric_distribution <unsigned> adist {0.5};
static std::geometric_distribution <unsigned> pdist {0.3};
static std::uniform_int_distribution <unsigned> tdist (5u, 500u);
static std::mt19937 gen;


void task (unsigned priority, unsigned delay)
{
    std::this_thread::sleep_for (std::chrono::milliseconds (delay));
    {
        static std::mutex m;
        std::lock_guard <std::mutex> lock (m);
        std::cout << "[priority: " << priority
                  << "; delay: " << delay << "ms"
                  << "; queued: " << task_queue.queued () << "]\n";
    }

    auto const add = adist (gen);
    for (unsigned i = 0; i < add; ++i) {
        auto const new_priority = pdist (gen);
        auto const new_delay = tdist (gen);
        task_queue.push (new_priority, task, new_priority, new_delay);
    }
}

int main (void)
{
    {
        std::random_device rd;
        std::array <std::uint_fast32_t, std::mt19937::state_size> state;
        std::generate (state.begin (), state.end (), [&rd] (void) { return rd (); });
        std::seed_seq sseq (state.begin (), state.end ());
        gen.seed (sseq);
    }

    for (std::size_t i = 0; i < initial_count; ++i) {
        auto const priority = pdist (gen);
        auto const delay = tdist (gen);
        task_queue.push (priority, task, priority, delay);
    }

    while (true) {}
    return 0;
}
