//
// dsa is a utility library of data structures and algorithms built with C++11.
// This file (priority_task.hpp) is part of the dsa project.
//
// author: Dalton Woodard
// contact: daltonmwoodard@gmail.com
// repository: https://github.com/daltonwoodard/priority_task.git
// license:
//
// Copyright (c) 2016 DaltonWoodard. See the COPYRIGHT.md file at the top-level
// directory or at the listed source repository for details.
//
//      Licensed under the Apache License. Version 2.0:
//          https://www.apache.org/licenses/LICENSE-2.0
//      or the MIT License:
//          https://opensource.org/licenses/MIT
//      at the licensee's option. This file may not be copied, modified, or
//      distributed except according to those terms.
//

#ifndef DSA_PRIORITY_TASK_HPP
#define DSA_PRIORITY_TASK_HPP

#include <algorithm>
#include <atomic>
#include <chrono>
#include <condition_variable>
#include <cstddef>
#include <cstdint>
#include <deque>
#include <exception>
#include <functional>
#include <future>
#include <memory>
#include <mutex>
#include <queue>
#include <thread>
#include <utility>
#include <vector>
#include "utilities/functions.hpp"
#include "utilities/sequence.hpp"


namespace dsa
{
    /*
     * task; a type-erased, allocator-aware std::packaged_task that also
     * contains its own arguments. The underlying packaged_task and the stored
     * argument tuple can be heap allocated or allocated with a provided
     * allocator.
     *
     * There is a single helper method for creating task objects: make_task,
     * which returns a pair of the newly constructed task and a std::future
     * object to the return value.
     */
    template <class Priority>
    class priority_task
    {
    public:
        priority_task (void) = default;
        ~priority_task (void) = default;

        priority_task (priority_task const &) = default;
        priority_task (priority_task &&) noexcept = default;

        priority_task & operator= (priority_task const &) = default;
        priority_task & operator= (priority_task &&) noexcept = default;

        void swap (priority_task & other) noexcept
        {
            std::swap (this->_p, other._p);
            std::swap (this->_t, other._t);
        }

        operator bool (void) const noexcept
        {
            return static_cast <bool> (this->_t);
        }

        template <class, class, class>
        friend class priority_task_system;

        template <class P, class F, class ... Args>
        friend std::pair <
            priority_task <P>,
            std::future <typename std::result_of <F (Args...)>::type>
        > make_priority_task (P p, F && f, Args && ... args)
        {
            using pair_type = std::pair <
                priority_task <P>,
                std::future <typename std::result_of <F (Args...)>::type>
            >;
            using model_type = task_model <
                typename std::result_of <F (Args...)>::type (Args...)
            >;

            priority_task <P> t (
                p, std::forward <F> (f), std::forward <Args> (args)...
            );
            auto fut = dynamic_cast <model_type const &> (*t._t).get_future ();
            return pair_type (std::move (t), std::move (fut));
        }

        template <class Allocator, class P, class F, class ... Args>
        friend std::pair <
            priority_task <P>,
            std::future <typename std::result_of <F (Args...)>::type>
        > make_priority_task (std::allocator_arg_t, Allocator const & alloc,
                              P p, F && f, Args && ... args)
        {
            using pair_type = std::pair <
                priority_task <P>,
                std::future <typename std::result_of <F (Args...)>::type>
            >;
            using model_type = task_model <
                typename std::result_of <F (Args...)>::type (Args...)
            >;

            priority_task <P> t (
                std::allocator_arg_t (), alloc, p,
                std::forward <F> (f), std::forward <Args> (args)...
            );
            auto fut = dynamic_cast <model_type const &> (*t._t).get_future ();
            return pair_type (std::move (t), std::move (fut));
        }

        void operator() (void) const
        {
            if (this->_t)
                this->_t->invoke_ ();
            else
                throw std::logic_error ("bad task access");
        }

    private:
        template <class F, class ... Args>
        priority_task (Priority p, F && f, Args && ... args)
            : _p (p)
            , _t (
                std::make_shared <task_model <
                    typename std::result_of <F (Args...)>::type (Args...)
                >> (std::forward <F> (f), std::forward <Args> (args)...)
            )
        {}

        template <class Allocator, class F, class ... Args>
        priority_task (std::allocator_arg_t,
                       Allocator const & alloc,
                       Priority p,
                       F && f, Args && ... args)
            : _p (p)
            , _t (
                std::make_shared <task_model <
                    typename std::result_of <F (Args...)>::type (Args...)
                >> (std::allocator_arg_t (), alloc,
                    std::forward <F> (f), std::forward <Args> (args)...)
            )
        {}

        struct task_concept
        {
            virtual ~task_concept (void) noexcept {}
            virtual void invoke_ (void) const = 0;
        };

        template <class> struct task_model;

        /*
         * tasks are assumed to be immediately invokable; that is,
         * invoking the underlying pakcaged_task with the provided arguments
         * will not block.
         */
        template <class R, class ... Args>
        struct task_model <R (Args...)> : task_concept
        {
            template <class F>
            explicit task_model (F && f, Args && ... args)
                : _f    (std::forward <F> (f))
                , _args (std::forward <Args> (args)...)
            {}

            template <class Allocator, class F>
            explicit task_model (
                std::allocator_arg_t, Allocator const & alloc,
                F && f, Args && ... args
            )
                : _f    (std::allocator_arg_t (), alloc, std::forward <F> (f))
                , _args (std::allocator_arg_t (), alloc,
                         std::forward <Args> (args)...)
            {}

            std::future <R> get_future (void) const
            {
                return this->_f.get_future ();
            }

            void invoke_ (void) const override
            {
                utility::apply (this->_f, this->_args);
            }

        private:
            std::packaged_task <R (Args...)> mutable _f;
            std::tuple <Args...> mutable _args;
        };

        Priority _p {};
        std::shared_ptr <task_concept const> _t;
    };

    /*
     * priority_task_system; a work-stealing, generic priority sorted tasking
     * system partly inspired by Sean Parent's "Better Code: Concurrency" talk;
     * see http://sean-parent.stlab.cc
     */
    template <
        class Priority,
        class Compare = std::less <Priority>,
        class TaskAllocator = std::allocator <priority_task <Priority>>
    >
    class priority_task_system
    {
        using compare_type = Compare;
        using priority_type = Priority;
        using task_type = priority_task <Priority>;
        using queue_container = std::deque <task_type>;

        struct task_compare
        {
            compare_type cmp_;
                
            explicit task_compare (compare_type const & cmp = compare_type ())
                : cmp_ (cmp)
            {}

            bool operator() (task_type const & lhs, task_type const & rhs)
            {
                return this->cmp_ (lhs._p, rhs._p);
            }
        };

        class task_queue
        {
            std::priority_queue <task_type, queue_container, task_compare>
                tasks_;
            std::condition_variable cv_;
            std::mutex mutex_;
            std::atomic_bool done_ {false};

        public:
            task_queue (void)
                : task_queue (Compare ())
            {}

            task_queue (Compare const & cmp)
                : tasks_ (task_compare {cmp})
            {}

            task_queue (task_queue const &) = delete;

            task_queue (task_queue && other) noexcept
                : tasks_ (std::move (other).tasks_)
                , done_  (other.done_.load ())
            {}

            void set_done (void)
            {
                this->done_.store (true);
                this->cv_.notify_all ();
            }

            task_type try_pop (void)
            {
                std::unique_lock <std::mutex>
                    lock (this->mutex_, std::try_to_lock);
                if (!lock) {
                    return task_type {};
                } else if (this->tasks_.empty ()) {
                    return task_type {};
                } else {
                    task_type t = this->tasks_.top ();
                    this->tasks_.pop ();
                    return std::move (t);
                }
            }

            bool try_push (task_type & t)
            {
                {
                    std::unique_lock <std::mutex>
                        lock (this->mutex_, std::try_to_lock);
                    if (!lock)
                        return false;

                    this->tasks_.emplace (std::move (t));
                }

                this->cv_.notify_one ();
                return true;
            }

            task_type pop (void)
            {
                std::unique_lock <std::mutex> lock (this->mutex_);
                while (this->tasks_.empty () && !this->done_)
                    this->cv_.wait (lock);

                if (this->tasks_.empty ())
                    return task_type {};

                task_type t = this->tasks_.top ();
                this->tasks_.pop ();
                return std::move (t);
            }

            void push (task_type t)
            {
                {
                    std::unique_lock <std::mutex> lock (this->mutex_);
                    this->tasks_.emplace (std::move (t));
                }
                this->cv_.notify_one ();
            }
        };

        std::vector <task_queue> queues_;
        std::vector <std::thread> threads_;
        std::vector <bool> exited_;
        typename std::allocator_traits <TaskAllocator>::
            template rebind_alloc <typename task_type::task_concept>
            alloc_;
        std::size_t nthreads_;
        std::size_t current_index_ {0};
        std::atomic_size_t queued_ {0};

        void run (std::size_t id)
        {
            while (true) {
                task_type task;

                for (std::size_t k = 0; k < 10 * this->nthreads_; ++k) {
                    task = this->queues_ [(id + k) % this->nthreads_].try_pop ();
                    if (task) {
                        this->queued_--;
                        goto call;
                    }
                }

                task = this->queues_ [id].pop ();
                if (task) {
                    this->queued_--;
                    goto call;
                } else {
                    goto finish;
                }

            call:
                task ();
                continue;
            }

        finish:
            /*
             * The done signal has been set and our queue is empty, but there
             * may still be queued tasks that must be completed, so try and
             * steal work from other queues before exiting.
             */
            while (this->queued_) {
                task_type task;

                for (std::size_t k = 0; k < this->nthreads_; ++k) {
                    task = this->queues_ [(id + k) % this->nthreads_].try_pop ();
                    if (task) {
                        this->queued_--;
                        task ();
                    }
                }

                std::this_thread::yield ();
            }

            this->exited_ [id] = true;
        }

        void join (void)
        {
            this->done ();
            for (auto & th : this->threads_)
                th.join ();
        }

    public:
        priority_task_system (void)
            : priority_task_system (std::thread::hardware_concurrency ())
        {}

        priority_task_system (std::size_t nthreads,
                              Compare const & cmp = Compare (),
                              TaskAllocator const & talloc = TaskAllocator ())
            : queues_   {}
            , threads_  {}
            , exited_   (nthreads, false)
            , alloc_    (talloc)
            , nthreads_ {nthreads}
        {
            this->queues_.reserve (nthreads);
            for (std::size_t th = 0; th < nthreads; ++th)
                this->queues_.emplace_back (cmp);

            this->threads_.reserve (nthreads);
            for (std::size_t th = 0; th < nthreads; ++th)
                this->threads_.emplace_back (
                    &priority_task_system::run, this, th
                );
        }

        ~priority_task_system (void)
        {
            this->join ();
        }

        std::size_t queued (void) const noexcept
        {
            return this->queued_.load ();
        }

        void done (void)
        {
            for (auto & q : this->queues_)
                q.set_done ();
        }

        void wait_to_completion (void)
        {
            while (true) {
                std::this_thread::yield ();

                if (this->queued_) {
                    continue;
                } else {
                    auto should_exit = true;
                    for (auto b : this->exited_)
                        should_exit &= b;
                    if (should_exit)
                        return;
                }
            }
        }

        void reset (void)
        {
            this->join ();
            this->threads_.clear ();
            this->queues_.clear ();
            this->queued_.store (0);

            for (std::size_t th = 0; th < this->nthreads_; ++th)
                this->queues_.emplace_back ();

            for (std::size_t th = 0; th < this->nthreads_; ++th)
                this->threads_.emplace_back (
                    &priority_task_system::run, this, th
                );
        }

        template <class F, class ... Args>
        auto push (priority_type p, F && f, Args && ... args)
            -> typename std::remove_reference <
                decltype (make_priority_task (
                    std::allocator_arg_t {}, this->alloc_, p,
                    std::forward <F> (f), std::forward <Args> (args)...
                ).second)
            >::type
        {
            auto t = make_priority_task (
                std::allocator_arg_t {}, this->alloc_, p,
                std::forward <F> (f), std::forward <Args> (args)...
            );

            auto const idx = this->current_index_++;
            for (std::size_t k = 0; k < 10 * this->nthreads_; ++k) {
                /*
                 * In order to maintain consistency we need to speculatively
                 * incremement the queued count and then decrement only if
                 * the try_push call failed. This is because the queued count
                 * must be incremented before a push and decremented only after
                 * a pop.
                 */
                this->queued_++;
                if (this->queues_ [(idx + k) % this->nthreads_]
                        .try_push (t.first)) {
                    return std::move (t.second);
                } else {
                    this->queued_--;
                }
            }

            this->queued_++;
            this->queues_ [idx % this->nthreads_].push (std::move (t.first));
            return std::move (t.second);
        }

        void push (task_type && t)
        {
            auto const idx = this->current_index_++;
            for (std::size_t k = 0; k < 10 * this->nthreads_; ++k) {
                /*
                 * In order to maintain consistency we need to speculatively
                 * incremement the queued count and then decrement only if
                 * the try_push call failed. This is because the queued count
                 * must be incremented before a push and decremented only after
                 * a pop.
                 */
                this->queued_++;
                if (this->queues_ [(idx + k) % this->nthreads_].try_push (t)) {
                    return;
                } else {
                    this->queued_--;
                }
            }

            this->queued_++;
            this->queues_ [idx % this->nthreads_].push (std::move (t));
        }
    };
}   // namespace dsa

#endif  // #ifndef DSA_PRIORITY_TASK_HPP
